<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','MailController@index')->name('getIndex');
Route::get('/create','MailController@getCreate')->name('getCreate');
Route::post('/store','MailController@postCreate')->name('postCreate');
Route::get('/show/{id}','MailController@show')->name('getMail');
