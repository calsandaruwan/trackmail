# Track Mail - application to track replies for system generated mails

> Application using IMAP and SMTP protocols. An System generated Id will be used to track replies. Files will be stored as JSON in Public/mail directory. Config app before using for the first time


- Clone and composer install
- Create (edit if exist) .env file in the root of project
- eaxmple values are given below
- must provide related values for MAIL_USERNAME=user@gmail.com andMAIL_PASSWORD=password
- Make sure that the public/mail directory is empty when using for the first time


> visit base url. Can create new email. Show previously created emails. Track if replied
