@extends('Common/layout')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>
                        <?= $data['title'] ?>
                        <a class="btn btn-info btn-sm pull-right" href="<?= '/' ?>">
                            <span class="glyphicon glyphicon-list" aria-hidden="true"></span>
                            List
                        </a>
                    </h4>
                </div>
                <div class="panel-body">
                    <p>To: {{  $data['result']->receiver }}</p>
                    <p>{{  $data['result']->message }}</p>
                </div>
            </div>

            <?php
            if($data['chain']){
                foreach($data['chain'] as $message){
                ?>
                <blockquote>
                    <small>
                        <div>{!!  $message['message']  !!}</div>
                    </small>
                </blockquote>
            <?php
                    }
                }
            ?>

        </div>
    </div>
@endsection