@extends('Common/layout')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>
                        <?= $data['title'] ?>
                        <a class="btn btn-info btn-sm pull-right" href="/create">
                            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                            Generate New Email
                        </a>
                    </h4>
                </div>
                <div class="panel-body">
                    <table class="table table-bordered table-condensed table-stripped">
                        <thead>
                            <th>Id</th>
                            <th>Subject</th>
                            <th>Receiver</th>
                            <th></th>
                        </thead>
                        <tbody>
                        <?php
                        if($data['result']) {
                        foreach ($data['result'] as $row) {
                        ?>
                        <tr id="row_<?= $row->id ?>">
                            <td><?= $row->id ?></td>
                            <td><?= $row->subject ?></td>
                            <td><?= $row->receiver ?></td>
                            <td>
                                <a href="/show/<?= $row->id ?>" class="btn btn-xs btn-success"><span class="glyphicon glyphicon-link" aria-hidden="true"></span></a>
                            </td>
                        </tr>
                        <?php
                        }
                        }else{
                            echo "<p>No mails available</p>";
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection