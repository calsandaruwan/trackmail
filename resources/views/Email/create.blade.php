@extends('Common/layout')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>
                        <?= $data['title'] ?>
                        <a class="btn btn-info btn-sm pull-right" href="/">
                            <span class="glyphicon glyphicon-list" aria-hidden="true"></span>
                            List
                        </a>
                    </h4>
                </div>
                <div class="panel-body">
                    <form method="post" action="/store">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="receiver">To:</label>
                                    <input name="post[receiver]" type="email" class="form-control" id="receiver"
                                           value="<?= isset($data['result']['receiver'])?($data['result']['receiver']):'' ?>"
                                    >
                                </div>
                                <div class="form-group">
                                    <label for="subject">Subject:</label>
                                    <input name="post[subject]" type="text" class="subject form-control" id="subject"
                                           value="<?= isset($data['result']['subject'])?($data['result']['subject']):'' ?>"
                                    >
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="date">Message:</label>
                                    <textarea rows="5" class="form-control" name="post[message]"><?= isset($data['result']['message'])?($data['result']['message']):'' ?></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <button name="submit" type="submit" class="btn btn-success pull-right">Send Email</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection