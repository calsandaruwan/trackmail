<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;


class MailController extends BaseController{

    private $filePath = '/mail/';

    public function __construct(){
    }

    public function index(){
        $files = File::allFiles(public_path().$this->filePath);
        $content = [];
        foreach($files as $file){
            $content[]= json_decode(File::get($file));
        }

        return View::make('Email.index',[
            'msg'=>['msg'=>'','content'=>''],
            'data'=>[
                'title'=>'System Generated Emails',
                'result'=>$content
            ]
        ]);
    }

    public function getCreate(){
        return View::make('Email.create',[
            'msg'=>['msg'=>'','content'=>''],
            'data'=>[
                'title'=>'Create New System Generated Email',
            ]
        ]);
    }

    public function postCreate(){

        $post = Input::get('post');
        //create unique id to identify emails
        $post['id'] = uniqid();

        $validation = Validator::make($post,[
            'receiver'=>'required',
            'subject'=>'required',
            'message'=>'required'
        ]);

        if(!$validation->fails()){
            Mail::send('Email.template', ['content' => $post], function ($m) use ($post) {
                $m->from(env('MAIL_USERNAME', ''), $post['id']);
                $m->to($post['receiver'], '')->subject($post['message']);
            });
            File::put(public_path().$this->filePath.$post['id'].".json",json_encode($post));
            $post='';

            $msgContent = "Successfully Generated the email";
            $msgType = 'success';
        }else{
            $msgContent = $validation->errors()->first();
            $msgType = 'danger';
        }

        return View::make('Email.create',[
            'msg'=>['content'=>$msgContent,'type'=>$msgType],
            'data'=>[
                'title'=>'Create New System Generated Email',
                'result'=>$post
            ]
        ]);
    }

    public function show($id){
        $content = json_decode(File::get(public_path().$this->filePath.$id.'.json'));

        //get relevent email thread with imap
        $hostname = env('IMAP_HOST', '');
        $username = env('MAIL_USERNAME', '');
        $password = env('MAIL_PASSWORD', '');

        /* try to connect */
        $conn = imap_open($hostname,$username,$password, OP_READONLY)
        or die('Cannot connect to Gmail: ' . imap_last_error());


        $emails = imap_search($conn,"TO $content->id" );
        $output = [];

        if($emails) {

            /* put the newest emails on top */
            rsort($emails);

            /* for every email... */
            foreach($emails as $email_number) {

                /* get information specific to this email */
                $output[]=[
                    'overview' => imap_fetch_overview($conn,$email_number,0),
                    'message' => imap_fetchbody($conn,$email_number,2)
                ];
            }
        }

        /* close the connection */

        imap_close($conn);
        return View::make('Email.show',[
            'data'=>[
                'title'=>$content->subject,
                'result'=>$content,
                'chain'=>$output
            ]
        ]);
    }

}
